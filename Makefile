# MAKEFILE FOR simple C++ programming
# orginally by David MacKay

# if you want the debugger kdbg to work nicely, REMOVE the -O2 flag
# if you want to get all warnings enabled, INCLUDE the -O2 flag

CFLAGS = $(INCDIRS)  \
	-pedantic -g -O2\
	-Wall -Wconversion\
	-Wformat  -Wshadow\
	-Wpointer-arith -Wcast-qual -Wwrite-strings\
	-D__USE_FIXED_PROTOTYPES__

LIBS = -l stdc++ -lm

CXX = g++

# Simple program
%: src/%.cc
	$(CXX) $(CFLAGS) $(LIBS) $< -o bin/$@

# Classes
bin/AdventCommon.o: src/AdventCommon.cc src/AdventCommon.h 
	$(CXX) $(CFLAGS) $(LIBS) -c src/AdventCommon.cc -o bin/AdventCommon.o

bin/ManVec.o: src/ManVec.cc src/ManVec.h 
	$(CXX) $(CFLAGS) $(LIBS) -c src/ManVec.cc -o bin/ManVec.o

bin/ManPar.o: src/ManPar.cc src/ManPar.h src/AdventCommon.h
	$(CXX) $(CFLAGS) $(LIBS) -c src/ManPar.cc -o bin/ManPar.o

bin/grid.o: src/grid.cc src/grid.h src/AdventCommon.h
	$(CXX) $(CFLAGS) $(LIBS) -c src/grid.cc -o bin/grid.o

# Programs using classes 
22objects = bin/22.o bin/22Func.o bin/AdventCommon.o bin/grid.o

22: $(22objects)
	$(CXX) $(CFLAGS) $(LIBS) -o bin/22 $(22objects)

bin/22.o: src/22.cc src/22Func.h 
	$(CXX) $(CFLAGS) $(LIBS) -c src/22.cc -o bin/22.o

bin/22Func.o: src/22Func.cc src/22Func.h src/AdventCommon.h src/grid.h
	$(CXX) $(CFLAGS) $(LIBS) -c src/22Func.cc -o bin/22Func.o

21objects = bin/21.o bin/21Func.o bin/AdventCommon.o bin/grid.o

21: $(21objects)
	$(CXX) $(CFLAGS) $(LIBS) -o bin/21 $(21objects)

bin/21.o: src/21.cc src/21Func.h 
	$(CXX) $(CFLAGS) $(LIBS) -c src/21.cc -o bin/21.o

bin/21Func.o: src/21Func.cc src/21Func.h src/AdventCommon.h src/grid.h
	$(CXX) $(CFLAGS) $(LIBS) -c src/21Func.cc -o bin/21Func.o

20objects = bin/20.o bin/20Func.o bin/AdventCommon.o bin/ManVec.o bin/ManPar.o

20: $(20objects)
	$(CXX) $(CFLAGS) $(LIBS) -o bin/20 $(20objects)

bin/20.o: src/20.cc src/20Func.h 
	$(CXX) $(CFLAGS) $(LIBS) -c src/20.cc -o bin/20.o

bin/20Func.o: src/20Func.cc src/20Func.h src/AdventCommon.h src/ManVec.h src/ManPar.h
	$(CXX) $(CFLAGS) $(LIBS) -c src/20Func.cc -o bin/20Func.o

19objects = bin/19.o bin/19Func.o bin/AdventCommon.o

19: $(19objects)
	$(CXX) $(CFLAGS) $(LIBS) -o bin/19 $(19objects)

bin/19.o: src/19.cc src/19Func.h 
	$(CXX) $(CFLAGS) $(LIBS) -c src/19.cc -o bin/19.o

bin/19Func.o: src/19Func.cc src/19Func.h src/AdventCommon.h 
	$(CXX) $(CFLAGS) $(LIBS) -c src/19Func.cc -o bin/19Func.o

18objects = bin/18.o bin/18Func.o bin/AdventCommon.o

18: $(18objects)
	$(CXX) $(CFLAGS) $(LIBS) -o bin/18 $(18objects)

bin/18.o: src/18.cc src/18Func.h 
	$(CXX) $(CFLAGS) $(LIBS) -c src/18.cc -o bin/18.o

bin/18Func.o: src/18Func.cc src/18Func.h src/AdventCommon.h 
	$(CXX) $(CFLAGS) $(LIBS) -c src/18Func.cc -o bin/18Func.o

17objects = bin/17.o bin/17Func.o bin/AdventCommon.o

17: $(17objects)
	$(CXX) $(CFLAGS) $(LIBS) -o bin/17 $(17objects)

bin/17.o: src/17.cc src/17Func.h 
	$(CXX) $(CFLAGS) $(LIBS) -c src/17.cc -o bin/17.o

bin/17Func.o: src/17Func.cc src/17Func.h src/AdventCommon.h 
	$(CXX) $(CFLAGS) $(LIBS) -c src/17Func.cc -o bin/17Func.o

16objects = bin/17.o bin/17Func.o bin/AdventCommon.o

16: $(16objects)
	$(CXX) $(CFLAGS) $(LIBS) -o bin/16 $(16objects)

bin/16.o: src/16.cc src/16Func.h 
	$(CXX) $(CFLAGS) $(LIBS) -c src/16.cc -o bin/16.o

bin/16Func.o: src/16Func.cc src/16Func.h src/AdventCommon.h 
	$(CXX) $(CFLAGS) $(LIBS) -c src/16Func.cc -o bin/16Func.o

15objects = bin/15.o bin/15Func.o 

15: $(15objects)
	$(CXX) $(CFLAGS) $(LIBS) -o bin/15 $(15objects)

bin/15.o: src/15.cc src/15Func.h 
	$(CXX) $(CFLAGS) $(LIBS) -c src/15.cc -o bin/15.o

bin/15Func.o: src/15Func.cc src/15Func.h
	$(CXX) $(CFLAGS) $(LIBS) -c src/15Func.cc -o bin/15Func.o

14objects = bin/14.o bin/14Func.o bin/10Func.o

14: $(14objects)
	$(CXX) $(CFLAGS) $(LIBS) -o bin/14 $(14objects)

bin/14.o: src/14.cc src/14Func.h src/10Func.h
	$(CXX) $(CFLAGS) $(LIBS) -c src/14.cc -o bin/14.o

bin/14Func.o: src/14Func.cc src/14Func.h src/10Func.h
	$(CXX) $(CFLAGS) $(LIBS) -c src/14Func.cc -o bin/14Func.o

13objects = bin/13.o bin/13Func.o 

13: $(13objects)
	$(CXX) $(CFLAGS) $(LIBS) -o bin/13 $(13objects)

bin/13.o: src/13.cc src/13Func.h 
	$(CXX) $(CFLAGS) $(LIBS) -c src/13.cc -o bin/13.o

bin/13Func.o: src/13Func.cc src/13Func.h
	$(CXX) $(CFLAGS) $(LIBS) -c src/13Func.cc -o bin/13Func.o

12objects = bin/12.o bin/12Func.o 

12: $(12objects)
	$(CXX) $(CFLAGS) $(LIBS) -o bin/12 $(12objects)

bin/12.o: src/12.cc src/12Func.h 
	$(CXX) $(CFLAGS) $(LIBS) -c src/12.cc -o bin/12.o

bin/12Func.o: src/12Func.cc src/12Func.h
	$(CXX) $(CFLAGS) $(LIBS) -c src/12Func.cc -o bin/12Func.o
12objects = bin/12.o bin/12Func.o 

11: $(11objects)
	$(CXX) $(CFLAGS) $(LIBS) -o bin/11 $(11objects)

bin/11.o: src/11.cc src/11Func.h 
	$(CXX) $(CFLAGS) $(LIBS) -c src/11.cc -o bin/11.o

bin/11Func.o: src/11Func.cc src/11Func.h
	$(CXX) $(CFLAGS) $(LIBS) -c src/11Func.cc -o bin/11Func.o

10objects = bin/10.o bin/10Func.o 

10: $(10objects)
	$(CXX) $(CFLAGS) $(LIBS) -o bin/10 $(10objects)

bin/10.o: src/10.cc src/10Func.h 
	$(CXX) $(CFLAGS) $(LIBS) -c src/10.cc -o bin/10.o

bin/10Func.o: src/10Func.cc src/10Func.h
	$(CXX) $(CFLAGS) $(LIBS) -c src/10Func.cc -o bin/10Func.o

09objects = bin/09.o bin/09Func.o 

09: $(09objects)
	$(CXX) $(CFLAGS) $(LIBS) -o bin/09 $(09objects)

bin/09.o: src/09.cc src/09Func.h 
	$(CXX) $(CFLAGS) $(LIBS) -c src/09.cc -o bin/09.o

bin/09Func.o: src/09Func.cc src/09Func.h
	$(CXX) $(CFLAGS) $(LIBS) -c src/09Func.cc -o bin/09Func.o

08objects = bin/08.o bin/08Func.o 

08: $(08objects)
	$(CXX) $(CFLAGS) $(LIBS) -o bin/08 $(08objects)

bin/08.o: src/08.cc src/08Func.h 
	$(CXX) $(CFLAGS) $(LIBS) -c src/08.cc -o bin/08.o

bin/08Func.o: src/08Func.cc src/08Func.h
	$(CXX) $(CFLAGS) $(LIBS) -c src/08Func.cc -o bin/08Func.o

07objects = bin/07.o bin/07Func.o 

07: $(07objects)
	$(CXX) $(CFLAGS) $(LIBS) -o bin/07 $(07objects)

bin/07.o: src/07.cc src/07Func.h 
	$(CXX) $(CFLAGS) $(LIBS) -c src/07.cc -o bin/07.o

bin/07Func.o: src/07Func.cc src/07Func.h
	$(CXX) $(CFLAGS) $(LIBS) -c src/07Func.cc -o bin/07Func.o

bin/Node.o: src/Node.cc src/Node.h 
	$(CXX) $(CFLAGS) $(LIBS) -c src/Node.cc -o bin/Node.o

bin/NodeSystem.o: src/NodeSystem.cc src/NodeSystem.h 
	$(CXX) $(CFLAGS) $(LIBS) -c src/NodeSystem.cc -o bin/NodeSystem.o

06objects = bin/06.o bin/06Func.o

06: $(06objects)
	$(CXX) $(CFLAGS) $(LIBS) -o bin/06 $(06objects)

bin/06.o: src/06.cc src/06Func.h
	$(CXX) $(CFLAGS) $(LIBS) -c src/06.cc -o bin/06.o

bin/06Func.o: src/06Func.cc src/06Func.h
	$(CXX) $(CFLAGS) $(LIBS) -c src/06Func.cc -o bin/06Func.o

05objects = bin/05.o bin/05Func.o

05: $(05objects)
	$(CXX) $(CFLAGS) $(LIBS) -o bin/05 $(05objects)

bin/05.o: src/05.cc src/05Func.h
	$(CXX) $(CFLAGS) $(LIBS) -c src/05.cc -o bin/05.o

bin/05Func.o: src/05Func.cc src/05Func.h
	$(CXX) $(CFLAGS) $(LIBS) -c src/05Func.cc -o bin/05Func.o

04objects = bin/04.o bin/04Func.o

04: $(04objects)
	$(CXX) $(CFLAGS) $(LIBS) -o bin/04 $(04objects)

bin/04.o: src/04.cc src/04Func.h
	$(CXX) $(CFLAGS) $(LIBS) -c src/04.cc -o bin/04.o

bin/04Func.o: src/04Func.cc src/04Func.h
	$(CXX) $(CFLAGS) $(LIBS) -c src/04Func.cc -o bin/04Func.o

03objects = bin/03.o bin/03Func.o

03: $(03objects)
	$(CXX) $(CFLAGS) $(LIBS) -o bin/03 $(03objects)

bin/03.o: src/03.cc src/03Func.h
	$(CXX) $(CFLAGS) $(LIBS) -c src/03.cc -o bin/03.o

bin/03Func.o: src/03Func.cc src/03Func.h
	$(CXX) $(CFLAGS) $(LIBS) -c src/03Func.cc -o bin/03Func.o

02objects = bin/02.o bin/02Func.o

02: $(02objects)
	$(CXX) $(CFLAGS) $(LIBS) -o bin/02 $(02objects)

bin/02.o: src/02.cc
	$(CXX) $(CFLAGS) $(LIBS) -c src/02.cc -o bin/02.o

bin/02Func.o: src/02Func.cc
	$(CXX) $(CFLAGS) $(LIBS) -c src/02Func.cc -o bin/02Func.o

01objects = bin/02.o bin/02Func.o

01: $(01objects)
	$(CXX) $(CFLAGS) $(LIBS) -o bin/01 $(01objects)

bin/01.o: src/01.cc
	$(CXX) $(CFLAGS) $(LIBS) -c src/01.cc -o bin/01.o

bin/01Func.o: src/01Func.cc
	$(CXX) $(CFLAGS) $(LIBS) -c src/01Func.cc -o bin/01Func.o

# Clean
clean: 
	rm bin/*.o
