/* 22.cc
 */

#include "22Func.h"

int main()
{
    grid infinite;

    Read(infinite);

    Result("The number of nodes that were infected is",
            Process(infinite, 10000));

    Result("The number of nodes that were infected is",
            Process2(infinite, 10000000));

    return 0;
}
