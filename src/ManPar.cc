/* Externally defined functions for ManPar.h
 */
#include "ManPar.h"

ManPar::ManPar() = default;
ManPar::ManPar(const int &i)
{
    ManVec m(i);
    xmvec = m; 
    vmvec = m; 
    amvec = m; 
}

ManVec& ManPar::x()
{ return xmvec; }

ManVec& ManPar::v()
{ return vmvec; }

ManVec& ManPar::a()
{ return amvec; }

void ManPar::Inc()
{
    for (unsigned u = 0; u < x().size(); ++u) {
        this->v()[u] += this->a()[u];
        this->x()[u] += this->v()[u];
    }
}

std::ostream& ManPar::Print(std::ostream &os)
{
    std::cout << "x "; xmvec.Print(os);
    std::cout << "v "; vmvec.Print(os);
    std::cout << "a "; amvec.Print(os);
    
    return os;
}

std::istream& ManPar::Get(std::istream &is) 
{
    std::string line;
    std::istream& ret = getline(is, line);
    std::istringstream iss(line);

    GetVec(iss, this->x());
    GetVec(iss, this->v());
    GetVec(iss, this->a());

    return ret;
}
