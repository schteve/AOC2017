/* Externally defined functions for 12Func.h
 */
#include "12Func.h"

unsigned CountGroups(uusmap pipes)
{
    unsigned count = 0;

    while (!pipes.empty()) {
        auto ret = CountElements(pipes, pipes.begin()->first);
        for (const auto &u : ret) {
            pipes.erase(u);
        }
        ++count;
    }

    return count;
}

void Add(uusmap &pipes, std::set<unsigned> &group, const unsigned &ID)
{
    for (const auto &u : pipes[ID]) {
        if (group.find(u) == group.end()) {
            group.insert(u);
            Add(pipes, group, u);
        }
    }
}

std::set<unsigned> CountElements(uusmap &pipes, const unsigned &ID)
{
    std::set<unsigned> group;

    Add(pipes, group, ID);

    return group;
}

void GetMap(uusmap &pipes)
{
    unsigned utemp;

    while (std::cin >> utemp) {
        pipes.insert({utemp, {}});

        std::string stemp;
        getline(std::cin, stemp);
        std::istringstream is(stemp);

        char c;
        std::string next = "";

        while (is >> c) {
            if (isdigit(c)) {
                next += c;
            } else if (next != "") {
                pipes[utemp].insert(std::stoi(next));
                next = "";
            }
        }
        if (next != "") {
            pipes[utemp].insert(std::stoi(next));
        }
    }
}
