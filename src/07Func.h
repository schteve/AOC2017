/* 07Func.h 
 */

#include <iostream>
#include <string>
#include <map>
#include <set>
#include <utility>
#include <cctype>

typedef std::string::iterator siter;

typedef std::pair<unsigned, std::set<std::string>> usspair;
typedef std::map<unsigned, std::set<std::string>> ussmap;

typedef std::pair<std::string, usspair> susppair;
typedef std::pair<const std::string, usspair> susppair2;
typedef std::map<std::string, usspair> suspmap;

void MakeMap(suspmap&);
void GetAlpha(siter&, siter&, std::string&);
unsigned GetWeight(siter&);
void Print(suspmap&);
std::string FindRoot(suspmap&);
unsigned Unbalanced(suspmap&);
unsigned FindOdd(susppair2&, suspmap&);
unsigned Weight(const std::string &name, suspmap &system);
void ShowWeights(const susppair&, suspmap&);
