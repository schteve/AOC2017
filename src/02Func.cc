/* Externally defined functions for 02Func.h
 */
#include "02Func.h"

int Read(std::istream &is, std::vector<std::vector<int>> &linevec)
{
    bool first = 1;
    int hi = 0, lo = 0, sum = 0;

    std::string line, numstr;

    while(getline(std::cin, line)) {
        line += ' ';
        std::vector<int> ivec;
        for (std::string::iterator 
                beg = line.begin(), end = line.end(); 
                beg != end; ++beg) {
            if (isdigit(*beg)) {
                numstr += *beg;
            } else if (!numstr.empty()) {
                int num = std::stoi(numstr);
                ivec.push_back(num);

                CheckReplace(numstr, num, hi, lo, first);
            }
        }
        SumReset(sum, hi, lo, first);
        linevec.push_back(ivec);
    }

    return sum;
}

void CheckReplace(std::string &numstr, int &num, int &hi, int &lo, bool &first)
{
    if ((hi < num) || first) {
        hi = num;
    } 
    if ((num < lo) || first) {
        lo = num;
    }

    numstr = "";
    if (first) {
        first = 0;
    }
}

void SumReset(int &sum, int &hi, int &lo, bool &first)
{
    sum += hi - lo;
    lo = 0;
    hi = 0;
    first = 1;
}

int Divisors(const std::vector<std::vector<int>> &linevec)
{
    int sum = 0;

    for (std::vector<int> ivec : linevec) {
        int num = 0, den = 1;

        for (unsigned a = 0; a < ivec.size() - 1; ++a){
            for (unsigned b = a + 1; b < ivec.size(); ++b) {
                int &aint = ivec[a];
                int &bint = ivec[b];

                if (aint%bint == 0) {
                    num = aint;
                    den = bint; 
                    break;
                } else if (bint%aint == 0) {
                    num = bint;
                    den = aint;
                    break;
                }
            }
        }

        sum += num/den;
    }

    return sum;
}
