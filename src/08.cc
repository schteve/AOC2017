/* 08.cc
 */

#include "08Func.h"

int main()
{
    std::map<std::string, int> registers;

    PrintResult(Process(registers), "");

    return 0;
}
