// grid.h
#ifndef GRID_H
#define GRID_H

#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

typedef unsigned un;
typedef const un coun;

class grid;

void SetStrSci(std::ostream &os);
std::ostream &operator<<(std::ostream&, const grid&);
// for maps
bool operator<(const grid&, const grid&);

class grid {
public:
    // (Delegating) Constructors
    grid(un, un);
    grid(un);
    grid();

    // Copy & Move
    grid(const grid&);
    grid& operator=(const grid&);
    ~grid();
    grid(grid&&);
    grid& operator=(grid&&);

    // Element Operations
    coun& height() const;
    coun& width() const;

    std::vector<un>::iterator xy(coun&, coun&);
    std::vector<un>::const_iterator xy(coun&, coun&) const;

    un& operator[](unsigned n)
    { return *xy(n%height(), n/height()); }
    const un& operator[](unsigned n) const
    { return *xy(n%height(), n/height()); }

    void Reset();
    void Print(std::ostream&) const;

    // Particle Operations
    bool CheckTrue(coun&, coun&, coun&, coun&);
    bool Set(coun&, coun&, coun&, coun&, coun&);
    bool PlaceBig(coun&, coun&);
    bool PlaceSma(coun&, coun&);
    bool UnPlaceSma(coun&, coun&);
private:
    un h = 10, w = h;
    std::vector<un> data;
};

inline coun& grid::height() const
    { return h; }
inline coun& grid::width() const
    { return w; }

inline std::vector<un>::iterator grid::xy(coun &x, coun &y)
    { return data.begin() + y * w + x; } // h*w grid 

inline std::vector<un>::const_iterator grid::xy(coun &x, coun &y) const
    { return data.begin() + y * w + x; } // h*w grid 

#endif
