/* 04Func.h 
 */

#include <iostream>
#include <sstream>
#include <string>
#include <utility>
#include <set>

template <typename T>
void WholeWords(const T &word, std::set<T> &wordset, unsigned &repeated)
{
    auto ret = wordset.insert(word);
    if (!ret.second) {
        repeated = 1;
    }
}

std::pair<unsigned, unsigned> CountValidPwds();
void Anagrams(const std::string&, std::set<std::multiset<char>>&, unsigned&);

inline
void Increment(unsigned &repeated, unsigned &count)
{
    if (repeated == 0) {
        ++count;
    } else {
        repeated = 0;
    }
}

