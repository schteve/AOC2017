/* 19.cc
 */

#include "19Func.h"

int main()
{
    unsigned count = 1;
    std::string ret = LetterOrder(count);

    Result("The order of the letters encountered is",
            ret);
    Result("The number of steps is", count);

    return 0;
}
