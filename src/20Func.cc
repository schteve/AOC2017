/* Externally defined functions for 20Func.h
 */

#include "20Func.h"

long Closest()
{
    long currind = 0, index = 0, curracc = 0, acc = 0;
    std::map<long, ManPar> Particles;

    ManPar p(3);
    while (p.Get(std::cin)) {
        Particles.insert({currind, p});

        curracc = labs(p.a().def());
        if (currind == 0 || curracc < acc) {
            acc = curracc;
            index = currind;
        }
        ++currind;
    }


    Result("The number of particles remaining is", 
            Collisions(Particles));
    
    return index;
}

unsigned long Collisions(std::map<long, ManPar> &Particles)
{
    std::cout << Particles.size() << std::endl;

    unsigned start = 10, u = start;

    while (u != 0) {
        std::map<ManVec, std::set<long>> Dups; 
        for (auto &pa : Particles) {
            pa.second.Inc();
            Dups[pa.second.x()].insert(pa.first);
        }

        //Print(Dups);

        for (auto &pb : Dups) {
            if (pb.second.size() > 1) {
                for (auto &l : pb.second) {
                    Particles.erase(l);
                }
                if (u != start + 1) {
                    u = start + 1;
                }
            }
        }

        --u;
    }

    return Particles.size();
}
