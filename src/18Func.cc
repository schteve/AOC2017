/* Externally defined functions for 18Func.h
 */

#include "18Func.h"

long long NoSent(const std::vector<std::string> &instructions)
{
    unsigned long long count = 0, zerocurr = 0, onecurr = 0; 
    long long ret0 = 0, ret1 = 0;
    cumap zeroregs, oneregs;
    zeroregs.insert({'p', 0});
    oneregs.insert({'p', 1});
    dull btzero, btone;

    do {
        unsigned count0 = 0, count1 = 0;
            //std::cout << "Prog Zero" << std::endl;
        while (ret0 != 2) {
            ret0 = Process(instructions, zerocurr, zeroregs, btzero, btone);
            ++count0;
        }

            //std::cout << "Prog One" << std::endl;
        while (ret1 != 2) {
            ret1 = Process(instructions, onecurr, 
                                oneregs, btone, btzero);
            if (ret1 == 1) {
                ++count;
            }
            ++count1;
        }

        if (count0 == 1 && count1 == 1) {
            break;
        }
        ret0 = ret1 = 0;
    } while ((onecurr < instructions.size()) 
            || (zerocurr < instructions.size()));

    return count;
}

long long FirstRecovered(const std::vector<std::string> &instructions)
{
    unsigned long long curr = 0, ret = 0;
    cumap regs;
    dull sent;

    while (curr < instructions.size()) {
        ret = Process(instructions, curr, regs, sent);

        if (ret != 0) {
            break;
        }
    }

    return ret;
}

void 
ProcessCommon(const std::vector<std::string> &instructions, 
        cumap &regs, std::string &reg, std::string &command, 
        std::string &num, unsigned long long &curr)
{
    GetParams(instructions[curr], regs, command, reg, num);

    ++curr;

    if (command == "set") {
        regs[reg[0]] = GetNum(regs, num);
    } else if (command == "add") {
        regs[reg[0]] += GetNum(regs, num);
    } else if (command == "mul") {
        regs[reg[0]] *= GetNum(regs, num);
    } else if (command == "mod") {
        regs[reg[0]] = regs[reg[0]]%GetNum(regs, num);
    } else if (command == "jgz") {
        if ((isalpha(reg[0]) && regs[reg[0]] > 0) || 
            (isdigit(reg[0]) && GetNum(regs, reg) > 0)) {
            curr += GetNum(regs, num) - 1;
        }
    }
}

long long 
Process(const std::vector<std::string> &instructions, 
        unsigned long long &curr, cumap &regs, 
        dull &in, dull &out)
{
    if (instructions.size() <= curr) {
        return 2;
    }

    std::string command, reg, num;
    ProcessCommon(instructions, regs, reg, command, num, curr);
    //Print(curr, command, reg, num, regs);

    if (command == "snd") {
        out.push_back(regs[reg[0]]);
        return 1;
    } else if (command == "rcv") {
        if (!in.empty()) {
            regs[reg[0]] = *in.begin();
            in.pop_front();
        } else {
            --curr;
            return 2;
        }
    }

    return 0;
}

long long 
Process(const std::vector<std::string> &instructions, 
        unsigned long long &curr, cumap &regs, dull &pn)
{
    std::string command, reg, num;
    ProcessCommon(instructions, regs, reg, command, num, curr);
    // Print(curr, command, reg, num, regs);

    if (command == "snd") {
        pn.push_back(regs[reg[0]]);
    } else if (command == "rcv") {
        if (regs[reg[0]] != 0) {
            unsigned long long freq;
            if (!pn.empty()) {
                freq = *(pn.end() - 1); 
                return freq;
            }
        }
    }

    return 0;
}
