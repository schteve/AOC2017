/* 09Func.h 
 */

#include <iostream>
#include <string>

unsigned NoOfGroups(const std::string&);
unsigned CleanString(std::string&);
unsigned Score(const std::string&);

inline
void Read(std::string &s) 
{
    std::string line;
    while (getline(std::cin, line)) {
        s += line;
    }
}
