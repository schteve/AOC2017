/* 02.cc
 */

#include "02Func.h"

int main()
{
    std::vector<std::vector<int>> ivec;

    std::cout << "Sum of line ranges: "
              << Read(std::cin, ivec) << std::endl;

    std::cout << "Sum of whole divisions: "
              << Divisors(ivec) << std::endl;

    return 0;
}
