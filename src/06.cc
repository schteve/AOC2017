/* 06.cc
 */

#include "06Func.h"

int main()
{
    std::vector<unsigned> uvec;
    Read(uvec);
    Print(uvec);
    std::cout << std::endl;

    auto ret = InfiniteLoops(uvec);
    std::cout << "Cycles to first evidence of repeat " 
              << ret.first << std::endl;
    std::cout << "Period of loop " 
            << InfiniteLoops(ret.second).first << std::endl;

    return 0;
}
