/* 21Func.h 
 */

#include <iostream>
#include <sstream>
#include <string>
#include <map>
#include "AdventCommon.h"
#include "grid.h"

typedef std::map<grid, grid> ggmap;

unsigned CountOn(ggmap&, const unsigned&);
void Process(grid&, ggmap&);
void ProSize(grid&, ggmap&, const unsigned&);
void MakeMap(ggmap&);
void Read(grid&, grid&, const std::string&);
void Insert(ggmap&, const grid&, const grid&);

inline
grid Mirror(const grid &g)
{
    unsigned width = g.width();
    grid temp(width);

    for (unsigned y = 0; y < width; ++y) {
        for (unsigned x = 0; x < width; ++x) {
            *temp.xy(x, y) = *g.xy(width - 1 - x, y);
        }
    }

    return temp;
}

inline
grid Rotate(const grid &g)
{
    unsigned width = g.width();
    grid temp(width);

    for (unsigned x = 0; x < width; ++x) {
        for (unsigned y = 0; y < width; ++y) {
            *temp.xy(y, width - 1 - x) = *g.xy(x, y);
        }
    }

    return temp;
}

inline
void RotThree(ggmap &r, grid &temp, const grid &t)
{
    r.insert({temp, t});
    for (unsigned u = 0; u < 3; ++u) {
        temp = Rotate(temp);
        r.insert({temp, t});
    }
}

inline
std::vector<unsigned> GetBinary(const std::string &line)
{
    std::vector<unsigned> uvec;
    std::istringstream is(line);
    char c;
    while (is >> c) {
        if (c == '.') {
            uvec.push_back(0);
        } else if (c == '#') {
            uvec.push_back(1);
        }
    }
    return uvec;
}

inline
void FindSizes(const std::vector<unsigned> &uvec,
        unsigned &fsize, unsigned &tsize)
{
    if (uvec.size() == 13) {
        fsize = 2; 
        tsize = 3;
    } else if (uvec.size() == 25) {
        fsize = 3;
        tsize = 4;
    }
}

inline
grid SubGrid(const grid &g, const unsigned &size, 
        const unsigned &X, const unsigned &Y)
{
    grid temp(size);

    for (unsigned x = 0; x < size; ++x) {
        for (unsigned y = 0; y < size; ++y) {
            *temp.xy(x, y) = *g.xy(X + x, Y + y);
        }
    }
    
    return temp;
}

inline
void AssignSub(grid &g, const grid &sub, 
        const unsigned &X, const unsigned &Y)
{
    for (unsigned x = 0; x < sub.height(); ++x) {
        for (unsigned y = 0; y < sub.height(); ++y) {
            *g.xy(X + x, Y + y) = *sub.xy(x, y); 
        }
    }
}

inline
void GridAssign(grid &g, const std::vector<unsigned> &uvec)
{
    unsigned sz = g.height();
    for (unsigned u = 0; u < sz * sz; ++u) {
       g[u] = uvec[u];
    }
}

inline
void InitArt(grid &a)
{
    std::vector<unsigned> uvec = {0, 1, 0, 0, 0, 1, 1, 1, 1};
    GridAssign(a, uvec);
}
