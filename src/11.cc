/* 11.cc
 */

#include "11Func.h"

int main()
{
    unsigned ret = GetSteps();
    
    std::cout << "The number of steps to the end point is " << std::endl
        << ret << std::endl;
    return 0;
}
