/* 01.cc
 */

#include "01Func.h"

int main()
{
    std::string input, line;
    unsigned opposum = 0;

    while (getline(std::cin, line)) {
        input += line;
    }

    std::cout << input << std::endl;

    std::cout << "Consecutive: " 
              << eval(input, opposum) << std::endl;

    if ((input.size() % 2) == 0) {
        std::cout << "Opposite: " << opposum << std::endl;
    }

    return 0;
}
