/* 15.cc
 */

#include "15Func.h"

int main()
{
    unsigned long a, b; 
    unsigned afactor = 16807, bfactor = 48271;
    Read(a, b);

    std::cout << a << " " << b << std::endl;

    std::string mess1 = "The number of matches after 4*10^7 iterations is";
    Result(mess1, Matches(a, afactor, b, bfactor));

    std::string mess2 = "The number of matches after 5*10^6 iterations is";
    Result(mess2, Matches2(a, afactor, b, bfactor));


    return 0;
}
