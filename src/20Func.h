/* 20Func.h 
 */

#include <iostream>
#include <sstream>
#include <map>
#include <set>
#include "AdventCommon.h"
#include "ManPar.h"

long Closest();
unsigned long Collisions(std::map<long, ManPar>&);

inline
void Print(const std::pair<ManVec, std::set<long>> &pa)
{
    pa.first.Print(std::cout);
    std::cout << "|";
    for (const long &l : pa.second) {
        std::cout << l << " ";
    }
    std::cout << std::endl;
}

inline
void Print(std::map<ManVec, std::set<long>> &Dups)
{
    for (auto &pa : Dups) {
        Print(pa);
    }
}
