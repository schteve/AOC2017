/* 15Func.h 
 */

#include <iostream>
#include <string>
#include <cctype>
#include <cmath>

template <typename T>
void Result(const std::string &mess, const T &ret)
{
    std::cout << mess << std::endl
        << ret << std::endl;
}

unsigned Matches(unsigned long, const unsigned&,
        unsigned long, const unsigned&);
unsigned Matches2(unsigned long, const unsigned&,
        unsigned long, const unsigned&);
void Check(const unsigned long&, const unsigned long&, unsigned&);
void Process(unsigned long&, const unsigned&);
void ReadNumber(unsigned long&);

inline
void Read(unsigned long &a, unsigned long &b)
{
    if (std::cin) {
        ReadNumber(a);
        ReadNumber(b);
    }
}

inline
std::string dectobinary(unsigned long dec, const int &dim)
{
    std::string binary;

    for (unsigned div = pow(2,dim - 1); div >= 1; div /= 2) {
        if (dec/div) {
            binary += "1";
            dec -= div;
        } else {
            binary += "0";
        }
    }

    return binary;
}
