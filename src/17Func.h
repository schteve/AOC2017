/* 17Func.h 
 */

#include <iostream>
#include <vector>
#include "AdventCommon.h"

unsigned long Second(const unsigned long&);
unsigned spinlock(const unsigned long&);

inline
void Print(const unsigned long &curr,
        const std::vector<unsigned> &uvec)
{
    std::cout << curr << "| ";
    for (auto a : uvec) {
        std::cout << a << " ";
    }
    std::cout << std::endl;
}
