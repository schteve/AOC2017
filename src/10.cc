/* 10.cc
 */

#include "10Func.h"

int main()
{
    std::string ret = Hash(std::cin);
    std::cout 
        << "The product of the first and second elements is " 
        << std::endl << ret << std::endl;

    return 0;
}
