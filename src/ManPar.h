/* ManPar.h 
 */
#ifndef MANPAR_H
#define MANPAR_H

#include <iostream>
#include <sstream>
#include "AdventCommon.h"
#include "ManVec.h"

class ManPar {
public:
    ManPar();
    ManPar(const int&);

    ManVec& x();
    ManVec& v();
    ManVec& a();
    void Inc();

    std::ostream& Print(std::ostream&);
    std::istream& Get(std::istream&);
private:
    ManVec xmvec, vmvec, amvec;
};

inline 
void GetVec(std::istream &is, ManVec &mvec)
{
    for (unsigned u = 0; u < 3; ++u) {
        mvec[u] = GetNum(is);
    }
}

#endif
