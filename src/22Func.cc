/* Externally defined functions for 22Func.h
 */

#include "22Func.h"

unsigned Process2(grid g, const unsigned &N) 
{
    int x = (g.width())/2, y = x, theta = 0;
    unsigned count = 0;

    for (unsigned u = 0; u < g.height() * g.width(); ++u) {
        if (g[u] == 1) {
            g[u] = 2;
        }
    }

    for (unsigned n = 0; n < N; ++n) {
        unsigned &state = *g.xy(x, y);
        if (state == 1) {
            ++count;
        }
        InfRot2(state, theta);
        Incxy(theta, x, y);
        IncSize(g, x, y);
    }
    
    return count;
}

unsigned Process(grid g, const unsigned &N) 
{
    int x = (g.width())/2, y = x, theta = 0;
    unsigned count = 0;

    for (unsigned n = 0; n < N; ++n) {
        unsigned state = *g.xy(x, y);
        if (!state) {
            ++count;
        }
        InfRot(++state%2, theta, g, x, y);
        Incxy(theta, x, y);
        IncSize(g, x, y);
    }

    return count;
}

void Read(grid &g)
{
    std::string line, input;

    while (getline(std::cin, line)) {
        input += line;
    }

    grid temp(sqrt(input.size()));
    GridAssign(temp, GetBinary(input));

    g = temp;
}
