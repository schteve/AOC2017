/* Externally defined functions for 10Func.h
 */
#include "10Func.h"

std::string Hash(std::istream &is)
{
    std::string clean;
    GetInputString(is, clean);

    return Hash(clean);
}

std::string Hash(const std::string &clean)
{
    constexpr unsigned poslen = 256;
    unsigned posns[poslen];

    InitArray(posns, poslen);
    ProcessHash(clean, posns, poslen);

    return Densify(posns, poslen);
}

std::string Densify(unsigned* posns, const unsigned &poslen)
{
    std::string hash = "";

    for (unsigned u = 0; u < 16; ++u) {
        unsigned bixor = *(posns + 16*u) ^ *(posns + 16*u + 1);
        for (unsigned v = 2; v < 16; ++v) {
            bixor ^= *(posns + 16*u + v);
        }
        hash += ToHex(bixor);
    }

    return hash;
}

void ProcessHash(const std::string &clean, 
        unsigned* posns, const unsigned &poslen)
{
    unsigned currpos = 0, skip = 0;

    for (unsigned g = 0; g < 64; ++g) {
        std::istringstream iss(clean);
        while (iss) {
            unsigned length = GetNextLength(iss);

            if (length > 1) {
                unsigned *subset = new unsigned[length]();
                InitSubset(posns, poslen, subset, currpos, length);
                RevSet(posns, poslen, subset, currpos, length);
            }

            currpos = (currpos + skip + length)%poslen;
            ++skip;
        }
    }
}

void InitSubset(unsigned *posns, const unsigned &poslen, unsigned *subset, 
        unsigned u, const unsigned &length)
{
    for (unsigned v = 0; v < length; ++v) {
        *(subset + v) = *(posns + u);
        u = (u+1)%poslen;
    }
}

void RevSet(unsigned *posns, const unsigned &poslen, unsigned *subset, 
        unsigned u, const unsigned &length)
{
    for (unsigned v = length; v > 0; --v) {
        *(posns + u) = *(subset + (v - 1));
        u = (u+1)%poslen;
    }
}
