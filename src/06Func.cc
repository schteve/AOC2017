/* Externally defined functions for 06Func.h
 */
#include "06Func.h"

std::pair<unsigned, std::vector<unsigned>> InfiniteLoops(std::vector<unsigned> &uvec)
{
    unsigned count = 0;
    std::vector<std::vector<unsigned>> uvecvec = { uvec };

    while ((uvecvec.size() == 1) || 
            (find(uvecvec.begin(), uvecvec.end(), *Last(uvecvec)) 
             == (Last(uvecvec)))) {
        std::vector<unsigned> temp = *Last(uvecvec);
        DoleOut(temp, FindLargest(temp));

        uvecvec.push_back(temp);
        ++count;
    }

    return std::make_pair(count, *Last(uvecvec));
}

std::vector<unsigned>::iterator FindLargest(std::vector<unsigned> &uvec)
{
    std::vector<unsigned>::iterator ret = uvec.begin(), trans = ret;

    while (trans != uvec.end()) {
        if (*ret < *trans) {
            ret = trans;
        }
        ++trans;
    }
    return ret;
}

void DoleOut(std::vector<unsigned> &temp, std::vector<unsigned>::iterator ret)
{
    unsigned dole = *ret;
    *ret = 0;

    while (dole != 0) {
        if (++ret == temp.end()) {
            ret = temp.begin();
        }
        ++*ret;
        --dole;
    }
}
