/* Externally defined functions for 04Func.h
 */
#include "04Func.h"

std::pair<unsigned, unsigned> CountValidPwds()
{
    unsigned count = 0, repeated = 0;
    unsigned count2 = 0, repeated2 = 0;
    std::string line;

    while (getline(std::cin, line)) {
        std::set<std::string> wordset;
        std::set<std::multiset<char>> charsset;
        std::istringstream is(line);
        std::string word;

        while (is >> word) {
            WholeWords(word, wordset, repeated);
            Anagrams(word, charsset, repeated2);
        }
        
        Increment(repeated, count);
        Increment(repeated2, count2);
    }
    std::cout << count2 << std::endl;

    return std::make_pair(count, count2);
}

void Anagrams(const std::string &word, std::set<std::multiset<char>> &charsset, 
        unsigned &repeated)
{
    std::istringstream ics(word);
    std::multiset<char> cset;
    char c;

    while (ics >> c) {
        cset.insert(c);
    }

    WholeWords(cset, charsset, repeated);
}
