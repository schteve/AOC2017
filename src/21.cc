/* 21.cc
 */

#include "21Func.h"

int main()
{
    ggmap rules;
    MakeMap(rules);

    Result("After five iterations, the number of elements\
 on is", CountOn(rules, 5));

    Result("After eighteen iterations, the number of elements\
 on is", CountOn(rules, 18));

    return 0;
}
