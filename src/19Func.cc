/* Externally defined functions for 19Func.h
 */

#include "19Func.h"

std::string LetterOrder(unsigned &count)
{
    std::string letters, map;
    unsigned long X = 0, Y = 0; 
    Read(map, X, Y);
    Print(map, X, Y);
    std::string::iterator b = map.begin();
    ul y = 0, x = map.find('|');
    char movbit = 's';

    while (Move(movbit, x, y)) {
        ++count;
        char here = *(b + xytoloc(x, y, X));
        std::cout << here << " " << movbit << " ";
        std::cout << x << " " << y << std::endl;
        if (here == '+') {
            Change(movbit, map, X, x, y);
        } else if (isalpha(here)) {
            letters.push_back(here);
            ul tempx = x, tempy = y;
            Move(movbit, tempx, tempy);
            if ( ' ' == *(b + xytoloc(tempx, tempy, X))) {
                Change(movbit, map, X, x, y);
            }
        }
    }

    return letters;
}
