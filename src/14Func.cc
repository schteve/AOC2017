/* Externally defined functions for 14Func.h
 */

#include "14Func.h"

unsigned Squares()
{
    std::string input, hex, binary;
    getline(std::cin, input);

    for (unsigned u = 0; u < 128; ++u) {
        std::string seed = input + "-" + std::to_string(u);
        hex += Hash(IpPro(seed));
    }

    binary =  hextobinary(hex);

    auto ret = Regions(binary);

    std::cout << "The total number of groups is"
        << std::endl << ret << std::endl;

    return CountOnes(binary);
}

unsigned Regions(std::string binary)
{
    unsigned count = 0;

    for (int y = 0; y < 128; ++y) {
        for (int x = 0; x < 128; ++x) {
            char &c = *(xy(binary, x,y));
            if (c == '1') {
                Recursion(binary, x, y);
                ++count;
                c = '0';

                /*
                std::cout << std::endl;
                Print(binary);
                std::cout << std::endl;
                */
            }
        }
    }

    return count;
}

void Recursion(std::string &binary, int x, int y)
{
    for (int v = -1; v <= 1; ++v) {
        for (int u = -1; u <= 1; ++u) {
            if ((abs(u) != abs(v)) && (x+u >= 0) && (x+u < 128) 
                    && (y+v >= 0) && (y+v < 128)) {
                char &c = *(xy(binary, x+u, y+v));
                if (c == '1') {
                    c = '0';
                    Recursion(binary, x+u, y+v);
                }
            }
        }
    }
}
