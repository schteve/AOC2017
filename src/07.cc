/* 07.cc
 */

#include "07Func.h"

int main()
{
    suspmap system;

    MakeMap(system);
    //Print(system);

    std::cout << std::endl << "The name of the root is "
        << FindRoot(system) << std::endl;

    auto ret = Unbalanced(system);
    std::cout << std :: endl 
        << "The required weight of the errant program is "
        << ret << std::endl;

    return 0;
}
