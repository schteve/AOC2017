/* Externally defined functions for 05Func.h
 */
#include "05Func.h"

unsigned Process(std::vector<int> ivec, const unsigned &which) {
    unsigned count = 0;
    std::vector<int>::size_type index = 0;

    while (index < ivec.size()) {
        switch (which) {
            case 1:
                MethodOne(ivec, index);
                break;
            case 2:
                MethodTwo(ivec, index);
                break;
        }
        ++count;
    }
    
    return count;
}

