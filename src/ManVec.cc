/* Externally defined functions for ManVec.h
 */
#include "ManVec.h"

bool operator<(const ManVec &lhs, const ManVec &rhs)
{
    if (lhs.size() == rhs.size()) {
        for (unsigned u = 0; u < lhs.size(); ++u) {
            if (lhs[u] != rhs[u]) {
                return lhs[u] < rhs[u];
            }
        }
        return 0;
    } else {
        return lhs.size() < rhs.size(); // avoid missing ret
    }
}

ManVec::ManVec(std::initializer_list<long> lil)
{
    for (auto b = lil.begin(), e = lil.end(); 
            b != e; ++b) {
        vec.push_back(*b);
    }
}

ManVec::ManVec(const unsigned &u)
{
    for (unsigned a = 0; a < u; ++a) {
        vec.push_back(0);
    }
}

std::ostream& ManVec::Print(std::ostream &os)
{
    for (const long &l : vec) {
        os << l << " ";
    }

    return os;
}

std::ostream& ManVec::Print(std::ostream &os) const
{
    for (const long &l : vec) {
        os << l << " ";
    }

    return os;
}

void ManVec::push_back(const long &l)
{
    vec.push_back(l);
}

long ManVec::def()
{
    long sum = 0;

    for (const long &l : vec) {
        sum += labs(l);
    }

    return sum;
}
