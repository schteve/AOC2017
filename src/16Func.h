/* 16Func.h 
 */

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>
#include "AdventCommon.h"

std::string Repetition(std::string&, std::string&);
void Dance(std::string&, std::istream&);
std::string Read(std::istream&);
void Process(std::string&, const std::string&);
void Exchange(std::string&, const std::string&);
void Partner(std::string&, const std::string&);
void Spin(std::string&, const std::string&);
