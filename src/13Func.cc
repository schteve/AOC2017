/* Externally defined functions for 13Func.h
 */

#include "13Func.h"

unsigned GreatEscape(upmap layers)
{
    unsigned count = 0;

    while ((layers.begin()->second.first == 0) 
            || (TopSeverity2(layers) != 0)) {
        IncrementState(count, layers);
    }

    return count;
}

unsigned TopSeverity2(upmap layers)
{
    unsigned pos = 0, severity = 0;

    while (severity == 0 && !layers.empty() && pos <= (--layers.end())->first) {
        Process2(layers, pos, severity);
        IncrementState(pos, layers);
        RemPrevLayer(layers, pos-1);
        //std::cout << std::endl;
    }

    return severity;
}

unsigned TopSeverity(upmap layers)
{
    unsigned pos = 0, severity = 0;

    while (!layers.empty() && pos <= (--layers.end())->first) {
        Process(layers, pos, severity);
        IncrementState(pos, layers);
        RemPrevLayer(layers, pos-1);
        //std::cout << std::endl;
    }

    return severity;
}

void Process2(upmap &layers, const unsigned &pos, unsigned &severity) 
{
    if (layers.find(pos) != layers.end()) {
        if (abs(layers[pos].first) == 0) {
            severity = 1;
        }
    }
}

void Process(upmap &layers, const unsigned &pos, unsigned &severity) 
{
    /*std::cout << pos << std::endl;
    PrintMap(layers); */
    if (layers.find(pos) != layers.end()) {
        if (abs(layers[pos].first) == 0) {
           /* std::cout << "Prod " << pos << " "
                << layers[pos].second << std::endl;*/
            severity += SevCalc(pos, layers[pos].second);
        }
    }
}

void GetMap(upmap &pipes)
{
    unsigned utemp;

    while (std::cin >> utemp) {
        pipes.insert({utemp, std::make_pair(0,1)});

        std::string stemp;
        getline(std::cin, stemp);
        std::istringstream is(stemp);

        char c;
        std::string next = "";

        while (is >> c) {
            if (isdigit(c)) {
                next += c;
            } else if (next != "") {
                ChangeSize(pipes, utemp, next);
                next = "";
            }
        }
        if (next != "") {
            ChangeSize(pipes, utemp, next);
        }
    }
}
