/* 20.cc
 */

#include "20Func.h"

int main()
{
    Result("At long times, the particle closest to the origin is", 
            Closest());

    return 0;
}
