/* Externally defined functions for 09Func.h
 */
#include "09Func.h"

unsigned NoOfGroups(const std::string &input)
{
    std::string dummy = input;
    unsigned ret = CleanString(dummy);
    std::cout << "The number of garbage characters is "
              << ret << std::endl;
    
    return Score(dummy);
}

unsigned Score(const std::string &clean)
{
    unsigned count = 0, openbraces = 0;

    for (const auto &c : clean) {
        if (c == '{') {
            ++openbraces;
        } else if (c == '}') {
            count += openbraces;
            --openbraces;
        }
    }

    return count;
}

unsigned CleanString(std::string &input)
{
    std::string clean;
    bool openangle = 0;
    unsigned garbagechars = 0;

    for (std::string::const_iterator beg = input.begin(), 
            end = input.end(); beg != end; ++beg) {

        switch (*beg) {
            case '<':
                if (openangle == 0) {
                    openangle = 1;
                } else {
                    ++garbagechars;
                }
                continue;
            case '>':
                if (openangle == 1) {
                    openangle = 0;
                    continue;
                }
                break;
            case '!':
                if (openangle == 1) {
                    ++beg;
                    continue;
                }
                break;
            case '{':
                if (openangle == 1) {
                    ++garbagechars;
                    continue;
                }
                break;
            case '}':
                if (openangle == 1) {
                    ++garbagechars;
                    continue;
                }
                break;
            default:
                if (openangle == 1) {
                    ++garbagechars;
                    continue;
                } else {
                    break;
                }
        }
        clean += *beg;
    }
    
    input = clean;
    std::cout << input << std::endl;

    return garbagechars;
}
