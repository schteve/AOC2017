/* 16.cc
 */

#include "16Func.h"

int main()
{
    const std::string TEST = "abcde",
        posns = "abcdefghijklmnop";
    std::string Aposns = posns, Bposns = posns;

    std::string key, line;
    while (getline(std::cin, line)) {
        key += line;
    }

    std::istringstream instruc(key);

    
    Dance(Aposns, instruc);
    Result("The string becomes:", Aposns);

    
    Result("The string becomes:", Repetition(Bposns, key));

    return 0;
}
