/* 22Func.h 
 */

#include <iostream>
#include <sstream>
#include <string>
#include <cmath>
#include "AdventCommon.h"
#include "grid.h"

void Read(grid &g);
unsigned Process(grid g, const unsigned&);
unsigned Process2(grid g, const unsigned&);

inline
void AngleRange(int &theta)
{
    if (theta < 0) {
        theta += 4;
    } else if (theta >= 4) {
        theta -= 4;
    }
}

inline
void Incxy(int &theta, int &x, int &y)
{
    const double pi = 3.1415926535;
    int dx = sin((theta/2.0)*pi), dy = -cos((theta/2.0)*pi);
    //std::cout << theta << " " << dx << " " << dy << std::endl;
    x += dx;
    y += dy;
}

inline
void InfRot2(unsigned &state, int &theta)
{
    //std::cout << b << std::endl;
    switch (state) {
        case 0:
            theta += -1;
            break;
        case 2:
            theta += 1;
            break;
        case 3:
            theta += 2;
            break;
    }
    AngleRange(theta);
    state = ++state % 4;
}

inline
void InfRot(const unsigned &b, int &theta, grid &g, 
        const int&x, const int &y)
{
    //std::cout << b << std::endl;
    theta += pow(-1, 2 + b);
    AngleRange(theta);
    *g.xy(x, y) = b;
}

inline
std::vector<unsigned> GetBinary(const std::string &line)
{
    std::vector<unsigned> uvec;
    std::istringstream is(line);
    char c;
    while (is >> c) {
        if (c == '.') {
            uvec.push_back(0);
        } else if (c == '#') {
            uvec.push_back(1);
        }
    }
    return uvec;
}

inline
void GridAssign(grid &g, const std::vector<unsigned> &uvec)
{
    unsigned sz = g.height();
    for (unsigned u = 0; u < sz * sz; ++u) {
       g[u] = uvec[u];
    }
}

inline
void AssignSub(grid &g, const grid &sub, 
        const unsigned &X, const unsigned &Y)
{
    for (unsigned x = 0; x < sub.width(); ++x) {
        for (unsigned y = 0; y < sub.height(); ++y) {
            *g.xy(X + x, Y + y) = *sub.xy(x, y); 
        }
    }
}

inline
void IncSize(grid &g, int &x, int &y)
{
    while ((x < 0) || (x >= g.width())) {
        grid temp(g.height(), g.width() + 1);
        if (x < 0) {
            AssignSub(temp, g, 1, 0);
            ++x;
        } else {
            AssignSub(temp, g, 0, 0);
        }
        g = temp;
    }
    while ((y < 0) || (y >= g.height())) { 
        grid temp(g.height() + 1, g.width());
        if (y < 0) {
            AssignSub(temp, g, 0, 1);
            ++y;
        } else {
            AssignSub(temp, g, 0, 0);
        }
        g = temp;
    }
}

