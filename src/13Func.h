/* 13Func.h 
 */

#include <iostream>
#include <sstream>
#include <string>
#include <map>
#include <cmath>
#include <utility>

typedef std::pair<int, unsigned> pair;
typedef std::map<unsigned, pair> upmap;

unsigned GreatEscape(upmap);
unsigned TopSeverity(upmap);
unsigned TopSeverity2(upmap);
void Process2(upmap&, const unsigned&, unsigned&);
void Process(upmap&, const unsigned&, unsigned&);
void GetMap(upmap&);

inline
void PrintMap(upmap& pipes) 
{
    for (auto p : pipes) {
        std::cout << p.first << " ";
        std::cout << p.second.first << " ";
        std::cout << p.second.second << " ";
        std::cout << std::endl;
    }
}

inline
void ChangeSize(upmap &pipes, const unsigned &utemp, const std::string &next)
{
    pipes[utemp].second = std::stoi(next);
}

inline 
unsigned SevCalc(const unsigned &depth, const unsigned range) 
{
    return depth * range;
}

inline
void IncrementState(unsigned &pos, upmap &layers)
{
    ++pos;
    for (auto &p : layers) {
        if (abs(++p.second.first) == p.second.second) {
            p.second.first = -(p.second.second - 2);
        }
    }
}

inline
void RemPrevLayer(upmap &layers, const unsigned &pos)
{
    if (layers.find(pos) != layers.end()) {
        layers.erase(pos);
    }
}
