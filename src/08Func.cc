/* Externally defined functions for 08Func.h
 * cat 08key | awk '{ print $6 }' | sort | uniq
 */
#include "08Func.h"

void PrintResult(const sipair &ret, const std::string &s)
{
    std::cout << "The largest " << s 
        << " value in any register is " 
        << std::endl << "\t"
        << ret.second << " in " << ret.first << std::endl;
}

sipair Process(simap &registers)
{
    PrintResult(GetMap(registers), "running");

    return GetLargest(registers);
}

sipair GetLargest(const simap &registers)
{
    auto beg = registers.begin(), end = registers.end();
    sipair lp = *beg;

    while (beg != end) {
        sipair trans = *beg;
        if (lp.second < trans.second) {
            lp = trans;
        }
        ++beg;
    }

    return lp;
}

sipair GetMap(simap &registers)
{
    std::map<std::string, std::function<int(int, int)>> binops = 
    {
        {"inc", std::plus<int>()},
        {"dec", std::minus<int>()} 
    };

    std::map<std::string, std::function<bool(int, int)>> unops = 
    {
        {"<", std::less<int>()},
        {"<=", std::less_equal<int>()},
        {">", std::greater<int>()},
        {">=", std::greater_equal<int>()},
        {"==", std::equal_to<int>()},
        {"!=", std::not_equal_to<int>()}
    };

    std::string line;
    sipair rlp;
    while (getline(std::cin, line)) {
        std::string A, B, updown, oper;
        int inc, test;
        ReadParams(line, A, B, updown, oper, inc, test);

        if (unops[oper](registers[B], test)) {
            registers[A] = binops[updown](registers[A], inc);
        }

        sipair rtrans = GetLargest(registers);
        if (rlp.first.empty() || rlp.second < rtrans.second) {
            rlp = rtrans;
        }
    }
    return rlp;
}

void ReadParams(const std::string &line, std::string &A, std::string &B, 
        std::string &updown, std::string &oper, int &inc, int &test)
{
    std::istringstream is(line);
    is >> A;
    is >> updown;
    std::string temp;
    is >> temp;
    inc = std::stoi(temp);
    is >> temp;
    is >> B;
    is >> oper;
    is >> temp;
    test = std::stoi(temp);
}
