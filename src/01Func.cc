/* Externally defined functions for 01Func.h
 */
#include "01Func.h"

unsigned eval(const std::string &s, unsigned &opposum) 
{
    unsigned curr = 0, hold = 0, sum = 0;
    
    // iterate across s
    for (std::string::const_iterator beg = s.begin(), 
            oppo = beg + s.size()/2, end = s.end(); 
            beg != end; ++beg) {
        if (*beg != ' ') { // ignoring spaces
            curr = char_to_int(beg); // convert 'n' to n
            if (beg != s.begin()) {     // need hold
                if (hold == curr) { // if curr == hold
                    sum += curr;    // increment sum with curr
                }
            }
            oppoinc(oppo, end, curr, opposum); // always run oppo
            hold = curr;    // reset hold
        }
    }

    unsigned first = char_to_int(s.begin());
    if (s.size() > 1 && hold == first ) { 
        sum += first;// include cyclic boundary condition
    }

    return sum;
}

void oppoinc(str_ci &oppo, const str_ci &end, 
        const unsigned &curr, unsigned &opposum)
{
    if (oppo != end) { // only increment oppo up to end
        if (curr == char_to_int(oppo)) {
            opposum += 2 * curr; // only have to process half
        }
        ++oppo;     // if here, always increment oppo
    }
}
