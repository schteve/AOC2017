/* 04.cc
 */

#include "04Func.h"

int main()
{
    auto ret = CountValidPwds();
    std::cout << "Number of lines with:" << std::endl;
    std::cout << "\tno repeating words " 
        << ret.first << std::endl;

    std::cout << "\tno words with the same set of letters " 
        << ret.second << std::endl;
    return 0;
}
