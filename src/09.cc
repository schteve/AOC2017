/* 09.cc
 */

#include "09Func.h"

int main()
{
    std::string input;
    Read(input);

    unsigned ret = NoOfGroups(input);
    std::cout << "The score is " 
              << ret << std::endl;

    return 0;
}
