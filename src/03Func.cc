/* Externally defined functions for 03Func.h
 */
#include "03Func.h"

void loctoxy(const unsigned &location, int &x, int &y)
{
    // find which `shell' location is in
    unsigned upper = 1, lower = 1;
    FindShell(upper, lower, location);

    // starting location of that shell: 2, 10, 26, etc.
    ShellStartPos(x, y, upper);

    // difference between start of shell and location
    unsigned diff = GetDiff(location, (CumuShellTotal(lower) + 1));

    // Move there around the shell
    MoveAroundShell(x, y, diff, upper);
}

int ManhattanDistance(const unsigned &location)
{
    unsigned distance = 0;

    if (location == 0) {
        return -1;
    } else if (location > 1) {
        int x = 1, y = 0;

        loctoxy(location, x, y);
        // Distance in taxicab geometry
        distance = modx(x, y);
    }

    return distance;
}

unsigned xytoloc(const int &X, const int &Y)
{
    const unsigned upper = xytoshell(X, Y);

    unsigned diff = 0;
    int  x = 1, y = 0;
    ShellStartPos(x, y, upper);
    MoveAroundShell(x, y, X, Y, upper, diff);

    return (CumuShellTotal(upper - 1) + 1 + diff);
}

unsigned SpiralFibonacci(unsigned limit)
{
    std::vector<unsigned> TotsVec = {1};

    while (*(TotsVec.end() - 1) <= limit) {
        unsigned next = 0;
        int x, y;
        loctoxy(TotsVec.size() + 1, x, y);

        for (int i = -1; i <= 1; ++i) {
            for (int j = -1; j <= 1; ++j) {
                unsigned location = xytoloc(x+i, y+j);
                if (((i != 0) || (j != 0)) && (location <= TotsVec.size())) {
                        next += TotsVec[location - 1];
                    } 
            }
        }
        TotsVec.push_back(next);
    }

    return *(TotsVec.end() - 1);
}
