/* 11Func.h 
 */

#include <iostream>
#include <cmath>
#include <string>
#include <cctype>
#include <map>
#include <utility>

typedef std::pair<int, int> iipair;
typedef std::map<std::string, iipair> siipmap;

unsigned GetSteps();
void MakeMap(siipmap&);
void GetPos(iipair&, siipmap&);
unsigned GetSteps(iipair, siipmap&);

inline
void OnAxis(int &a, unsigned &count)
{
    count += abs(a);
    a = 0;
}

inline
void SameSign(int &a, int &b, unsigned &count)
{
    count += abs(a);
    b -= a;
    a = 0;
}
