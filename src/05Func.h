/* 05Func.h 
 */

#include <iostream>
#include <vector>
#include <cmath>

template <typename T>
void Read(std::vector<T> &vec)
{
    T dummy;

    while (std::cin >> dummy) {
        vec.push_back(dummy);
    }
}

unsigned Process(std::vector<int>, const unsigned&); 

inline
void MethodOne(std::vector<int> &ivec, std::vector<int>::size_type &index) 
{
    index += ivec[index]++;
}

inline
void MethodTwo(std::vector<int> &ivec, std::vector<int>::size_type &index) 
{
    std::vector<int>::size_type dummy = index;

    index += ivec[dummy];

    if (ivec[dummy] >= 3) {
        --ivec[dummy];
    } else {
        ++ivec[dummy];
    }
}
