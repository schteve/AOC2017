/* 19Func.h 
 */

#include <iostream>
#include <string>
#include "AdventCommon.h"

typedef unsigned long ul;

std::string LetterOrder(unsigned &count);

inline 
ul xytoloc(const ul &x, const ul &y, const ul &X)
{
    return y * X + x;
}

inline
void Change(char &movbit, const std::string &map, const ul &X, ul &x, ul &y)
{
    std::cout << "Change" << " " << movbit << std::endl;
    if (movbit == 'n' || movbit == 's') {
        ul a = xytoloc(x+1, y, X);
        ul b = xytoloc(x-1, y, X);
        std::cout << a << " " << b <<  " " << map.size() << std::endl;
        std::cout << map[a] << " " << map[b] << std::endl;

        if ((a < map.size()) && 
                ((map[a] == '-') || isalpha(map[a]))) {
            movbit = 'e';
        } else if ((b < map.size()) && 
                ((map[b] == '-') || isalpha(map[b]))) {
            movbit = 'w';
        } else {
            movbit = 'X';
        }
    } else if (movbit == 'e' || movbit == 'w') {
        ul a = xytoloc(x, y+1, X);
        ul b = xytoloc(x, y-1, X);

        if ((a < map.size()) && 
                ((map[a] == '|') || isalpha(map[a]))) {
            movbit = 's';
        } else if ((b < map.size()) && 
                ((map[b] == '|') || isalpha(map[b]))) {
            movbit = 'n';
        } else {
            movbit = 'X';
        }
    } else {
        movbit = 'X';
    }
    std::cout << std::endl;
}

inline
bool Move(char &movbit, ul &x, ul& y)
{
    switch (movbit) {
        case 'X':
            return 0;
        case 's':
            ++y;
            return 1;
        case 'n':
            --y;
            return 1;
        case 'e':
            ++x;
            return 1;
        case 'w':
            --x;
            return 1;
    }
    return 0;
}

inline
void Read(std::string &map, ul &X, ul &Y)
{
    std::string line;
    while (getline(std::cin, line)) {
        if (X == 0) {
            X = line.size();
        }
        ++Y;
        map += line;
    }
}

inline
std::string NthRow(const unsigned &u, const std::string &map,
        const ul &X, const ul &Y) 
{
    const auto b = map.begin();
    return std::string((b + u*X), (b + (X-1) + u*X));
}

inline 
void Print(std::string &map, ul &X, ul &Y)
{
    for (unsigned u = 0; u < Y; ++u) {
        std::cout << NthRow(u, map, X, Y) 
            << std::endl;
    }
}
