/* 18Func.h 
 */

#include <iostream>
#include <sstream>
#include <deque>
#include <string>
#include <vector>
#include <map>
#include "AdventCommon.h"

typedef std::map<char, long long> cumap;
typedef std::deque<long long> dull;

long long NoSent(const std::vector<std::string>&);
long long FirstRecovered(const std::vector<std::string>&);
void ProcessCommon(const std::vector<std::string>&, cumap&, 
        std::string &reg, std::string&, long long&,
        unsigned long long&);
long long Process(const std::vector<std::string>&, 
        unsigned long long&, cumap&, dull&, dull&);
long long Process(const std::vector<std::string>&, 
        unsigned long long&, cumap&, dull&);

inline
void GetParams(const std::string &instruction, cumap &regs,
        std::string &command, std::string &reg, std::string &num)
{
    std::istringstream iss(instruction);
    iss >> command;
    iss >> reg;
    iss >> num;
}

inline
long long GetNum(cumap &regs, const std::string &num)
{
    long long val = 0;

    if (!num.empty()) {
        if (isalpha(*num.begin())) {
            val = regs[num[0]];
        } else {
            val = std::stol(num);
        }
    }
    
    return val;
}

inline 
void Print(const unsigned long long &curr,
        const std::string &command, const std::string &reg,
        const std::string &num, cumap &regs)
{
    std::cout << curr << " " << command << " " << reg << " " 
        <<  GetNum(regs, num) << " "
        << reg[0] << " = " << regs[reg[0]] << std::endl;
}

