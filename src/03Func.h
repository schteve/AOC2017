/* 03Func.h 
 */

#include <iostream>
#include <cmath>
#include <vector>

void loctoxy(const unsigned&, int&, int&);
int ManhattanDistance(const unsigned&);
unsigned xytoloc(const int&, const int&);
unsigned SpiralFibonacci(unsigned);

inline 
unsigned CumuShellTotal(const unsigned &shell)
{
    if (shell == 1) {
        return 1;
    } else {
        // cumulative sum is 4n(n-1)+1
        return 4 * shell * (shell - 1) + 1;
    }
}

inline
void FindShell(unsigned &upper, unsigned &lower, const unsigned &location)
{
    while (CumuShellTotal(upper) < location) {
        lower = upper;
        ++upper;
    } // in shell number "upper"
}

inline
void ShellStartPos(int &x, int &y, const unsigned &upper)
{
    x = (upper - 1);
    y = (2 - upper); 
}

inline
unsigned GetDiff(const unsigned &B, const unsigned &A)
{
    // A -> B
    return B - A;
}

inline 
void Inc(int &ordinate, unsigned &diff, unsigned cnt)
{
    while (diff > 0 && cnt > 0) {
        ++ordinate;
        --diff;
        --cnt;
    }
}

inline 
void Dec(int &ordinate, unsigned &diff, unsigned cnt)
{
    while (diff > 0 && cnt > 0) {
        --ordinate;
        --diff;
        --cnt;
    }
}

inline 
unsigned Inc(int &ordinate, const int &coordinate, 
        const int &ORD, const int &COORD, 
        unsigned cnt)
{
    while ((cnt > 0) && ((ordinate != ORD) || (coordinate != COORD))) {
        ++ordinate;
        --cnt;
    }

    return cnt;
}

inline 
unsigned Dec(int &ordinate, const int &coordinate, 
        const int &ORD, const int &COORD, 
        unsigned cnt)
{
    while (((ordinate != ORD) || (coordinate != COORD)) && cnt > 0) {
        --ordinate;
        --cnt;
    }

    return cnt;
}

inline
void MoveAroundShell(int &x, int &y, unsigned &diff, const unsigned &upper)
{
    Inc(y, diff, 2*(upper - 1) - 1);
    Dec(x, diff, 2*(upper - 1));
    Dec(y, diff, 2*(upper - 1));
    Inc(x, diff, 2*(upper - 1));
}

inline
void MoveAroundShell(int &x, int &y, const int &X, const int &Y, 
        const unsigned &upper, unsigned &diff)
{
    unsigned side = 2*(upper - 1);

    diff += (side - 1) - Inc(y, x, Y, X, side - 1);
    diff += side - Dec(x, y, X, Y, side);
    diff += side - Dec(y, x, Y, X, side);
    diff += side - Inc(x, y, X, Y, side);
}

inline
unsigned modx(const unsigned &x, const unsigned &y)
{
    return abs(x) + abs(y);
}

inline
unsigned xytoshell(const int &X, const int &Y)
{
    unsigned upper;

    if (abs(X) >= abs(Y)) {
        upper = abs(X);
    } else {
        upper = abs(Y);
    }

    return upper + 1;
}
