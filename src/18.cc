/* 18.cc
 */

#include "18Func.h"

int main()
{
    std::vector<std::string> instructions;
    std::string line;

    while (getline(std::cin, line)) {
        instructions.push_back(line);
    }

    Result("The first recovered frequency is",
            FirstRecovered(instructions));
    std::cout << std::endl;
    Result("The first program sent ",
            NoSent(instructions));

    return 0;
}
