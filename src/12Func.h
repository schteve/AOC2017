/* 12Func.h 
 */

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <set>

typedef std::map<unsigned, std::set<unsigned>> uusmap;

unsigned CountGroups(uusmap);
void Add(uusmap&, std::set<unsigned>&, const unsigned&);
std::set<unsigned> CountElements(uusmap&, const unsigned&);
void GetMap(uusmap&);

inline
void PrintMap(uusmap& pipes) 
{
    for (auto p : pipes) {
        for (auto u : p.second) {
            std::cout << u << " ";
        }
        std::cout << std::endl;
    }
}
