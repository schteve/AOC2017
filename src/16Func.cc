/* Externally defined functions for 16Func.h
 */
#include "16Func.h"

std::string Repetition(std::string &letters, std::string &key)
{
    unsigned count = 0;
    std::vector<std::string> svec = {"abcdefghijklmnop"};
    do {
        std::istringstream instruc(key);
        Dance(letters, instruc);
        svec.push_back(letters);
        ++count;
    } while (letters != "abcdefghijklmnop");
    return svec[1000000000%count];
}

void Dance(std::string &letters, std::istream &instruc)
{
    while (instruc) {
        Process(letters, Read(instruc));
    }
}

void Process(std::string &l, const std::string &I)
{
    switch (I[0]) {
        case 's':
            Spin(l, I);
            break;
        case 'x':
            Exchange(l, I);
            break;
        case 'p':
            Partner(l, I);
            break;
    }
    //std::cout << l << " " << I << std::endl;
}

/*
void ProcessPerms(std::string &l, const std::string &I)
{
    switch (I[0]) {
        case 's':
            Spin(l, I);
            break;
        case 'x':
            Exchange(l, I);
            break;
    }
    //std::cout << l << " " << I << std::endl;
}
void ProcessSwaps(std::string &l, const std::string &I)
{
    if (I[0] == 'p') {
        Partner(l, I);
    }
}
*/

void Exchange(std::string &l, const std::string &I)
{
    unsigned a, b;
    std::istringstream is(I);
    GetNum(is, a);
    GetNum(is, b);
    char temp = l[a];
    l[a] = l[b];
    l[b] = temp;
}

void Partner(std::string &l, const std::string &I)
{
    char aval = I[1], bval = I[3];
    unsigned long aind = l.find(aval), bind = l.find(bval);

    l[aind] = bval;
    l[bind] = aval;
}

void Spin(std::string &l, const std::string &I)
{
    unsigned skip = std::stoi(
            std::string(I.begin() + 1, I.end()));
    std::string s(l.size(),'z');

    for (unsigned u = 0; u < l.size(); ++u) {
        s[(u+skip)%l.size()] = l[u];
    }

    l = s;

    /*
    unsigned skip = std::stoi(
            std::string(I.begin() + 1, I.end())), curr = 0;
    char a = l[curr], b;
    do {
        unsigned next = (curr + skip)%l.size();
        b = l[next];
        l[next] = a;
        a = b;
        curr = next;
    } while (curr != 0);
    */
}

std::string Read(std::istream &instruc)
{
    char c;
    std::string ret;

    while (instruc >> c && c != ',') {
        ret += c;
    }

    return ret;
}
