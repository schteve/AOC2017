/* 14Func.h 
 */

#include <iostream>
#include <string>
#include <cctype>
#include <cmath>
#include "10Func.h"

unsigned Squares();
unsigned Regions(std::string binary);
void Recursion(std::string &binary, int x, int y);

inline
std::string::iterator xy(std::string &binary, const int &x, const int &y)
{
    return (binary.begin() + x + 128 * y);
}

inline 
void Print(std::string &binary)
{
    for (int y = 0; y < 8; ++y) {
        for (int x = 0; x < 8; ++x) {
            std::cout << *xy(binary, x, y) << " ";
        }
        std::cout << std::endl;
    }
}

inline
unsigned CountOnes(const std::string &binary)
{
    unsigned count = 0;
    for (const auto &c : binary) {
        if (c == '1') {
            ++count;
        }
    } 

    return count;
}

inline
std::string dectobinary(int dec)
{
    std::string binary;

    for (unsigned div = 8; div >= 1; div /= 2) {
        if (dec/div) {
            binary += "1";
            dec -= div;
        } else {
            binary += "0";
        }
    }

    return binary;
}

inline
int hextodec(const char &c)
{
    int dec;

    if (isalpha(c)) {
        dec = c;
        dec -= 87;
    } else {
        dec = c - '0';
    }

    return dec;
}

inline
std::string hextobinary(const char &c)
{

    return dectobinary(hextodec(c));
}

inline
std::string hextobinary(const std::string &s)
{
    std::string binary;
    
    for (const auto &c : s) {
        binary += hextobinary(c);
    }
    
    return binary;
}
