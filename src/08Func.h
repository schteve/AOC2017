/* 08Func.h 
 */

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <functional>

typedef std::map<std::string, int> simap;
typedef std::pair<std::string, int> sipair;

void PrintResult(const sipair&, const std::string&);
sipair Process(simap&);
sipair GetLargest(const simap&);
sipair GetMap(simap&);
void ReadParams(const std::string&, std::string&, std::string&, std::string&, 
        std::string&, int&, int&);
