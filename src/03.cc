/* 03.cc
 */

#include "03Func.h"

int main()
{
    unsigned location = 0;

    do { 
        if (location != 0) {
            break;
        }
        std::cout << "Enter a memory location" << std::endl;
    } while (std::cin >> location);


    std::cout << "Distance of " << location << " from origin is "
        << ManhattanDistance(location) << std::endl;

   std::cout << "First entry greater than " << location << " is "
      << SpiralFibonacci(location) << std::endl;

    return 0;
}
