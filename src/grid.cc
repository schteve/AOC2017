// Externally defined functions for grid.h

#include "grid.h"

// Non-member functions
void SetStrSci(std::ostream &os)
{
    //os.precision(8);
    os.width(10); // 1. [.8.] e+??
    //os << std::scientific;
}

std::ostream &operator<<(std::ostream &os, const grid &g)
{
    g.Print(os);
    return os;
}

bool operator<(const grid &lhs, const grid &rhs)
{
    if (lhs.height() == rhs.height()) {
        unsigned sz = lhs.height();
        for (unsigned u = 0; u < sz * sz; ++u) {
            if (lhs[u] != rhs[u]) {
                return lhs[u] < rhs[u];
            }
        }
        return 0;
    } else {
        return lhs.height() < rhs.height(); // avoid missing ret
    }
}

// (Delegating) Constructors
grid::grid(unsigned H, unsigned W):
    h(H), w(W)
{
    data.reserve(h*w);
    for (unsigned u = 0; u < h*w; ++u) {
        data.push_back(0); // h*w element lattice
    }
}

grid::grid(unsigned L): grid(L, L) {}

grid::grid(): grid(10, 10) {}

// Copy & Move
grid::grid(const grid &rhs):
    h(rhs.h), w(rhs.w), data(rhs.data) {}

grid& grid::operator=(const grid &rhs)
{
    h = rhs.h;
    w = rhs.w;
    data = rhs.data;
    return *this;
}

grid::~grid() { }

grid::grid(grid &&rhs):
    h(rhs.h), w(rhs.w)
{ data = std::move(rhs.data); } 

grid& grid::operator=(grid &&rhs)
{
    if (this != &rhs) {
        h = rhs.h;
        w = rhs.w;
        data = std::move(rhs.data);
    }
    return *this;
}

// Element Operations

void grid::Reset()
{
    for (auto &u : data) {
        if (u != 0) {
            u = 0;
        }
    }
}

void grid::Print(std::ostream &os) const
{
    for (unsigned y = 0; y < h; ++y) {
        for (unsigned x = 0; x < w; ++x) {
            //SetStrSci(os);
            os << *xy(x, y) << " ";
        }
        os << std::endl;
    }
}

// Particle Operations
bool grid::CheckTrue(const unsigned &x, const unsigned &y, 
        const unsigned &a, const unsigned &b)
{
    std::vector<unsigned> areavec;

    // Copy the area in grid of interest into areavec
    for (unsigned i = 0; i < a; ++i) {
        copy(xy(x, y + i), xy(x + a, y + i), 
                back_inserter(areavec));
    }

    std::vector<unsigned> testvec(areavec.size(), 0);

    // Compare with a reference vector of zeros
    if (testvec == areavec) {
        return 1;
    } else {
        return 0;
    }
}

bool grid::Set(const unsigned &x, const unsigned &y, 
    const unsigned &a, const unsigned &b, const unsigned &val)
{
    // If far enough away from walls, set area axb from (x,y) to true
    if (x < (w-(a-1)) && y < (h-(b-1))) {
        for (unsigned v = y; v < y + b; ++v) {
            for (unsigned t = x; t < x + a; ++t) {
                *xy(t, v) = val;
            }
        }
        return 1;
    } else {
        return 0;
    }
}

bool grid::PlaceBig(const unsigned &x, const unsigned &y)
{
    Reset(); // Reset everything on changing large particle 
    // location: hence no checking

    const unsigned Bh = 4, Bw = Bh;
    return Set(x, y, Bw, Bh, 1);
}

bool grid::PlaceSma(const unsigned &x, const unsigned &y)
{
    const unsigned Sh = 2, Sw = Sh;
    if (CheckTrue(x, y, Sh, Sw)) {
        return Set(x, y, Sh, Sw, 1);
    } else {
        return 0;
    }
}

bool grid::UnPlaceSma(const unsigned &x, const unsigned &y)
{
    const unsigned Sh = 2, Sw = Sh;
    return Set(x, y, Sh, Sw, 0);
}

