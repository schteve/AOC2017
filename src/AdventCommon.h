/* AdventCommon.h 
 */

#ifndef ADVENTCOMMON_H
#define ADVENTCOMMON_H

#include <iostream>
#include <string>

template <typename T> inline void Result(const std::string &mess, const T &ret)
{
    std::cout << mess << std::endl
        << ret << std::endl;
}

inline
void GetNum(std::istream& is, unsigned &a)
{
    char c;
    std::string num = "";

    while (is >> c) {
        if (isdigit(c)) {
            num += c;
        } else if (!num.empty()) {
            break;
        }
    }
    a = std::stoi(num);
}

inline
long GetNum(std::istream& is)
{
    char c;
    std::string num = "";

    while (is >> c) {
        if (isdigit(c) || c == '-') {
            num += c;
        } else if (!num.empty()) {
            break;
        }
    }
    if (!num.empty()) {
        return std::stol(num);
    } else {
        return 0;
    }
}

#endif
