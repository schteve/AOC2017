/* 06Func.h 
 */

#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>

template <typename T>
void Read(std::vector<T> &vec)
{
    T dummy;

    while (std::cin >> dummy) {
        vec.push_back(dummy);
    }
}

template <typename T>
void Print(const std::vector<T> &vec)
{
    for (const auto element : vec) {
        std::cout << element << " ";
    }
    std::cout << std::endl;
}

template <typename T>
void PrintVec(const std::vector<T> &vecvec)
{
    for (const auto vec : vecvec) {
        Print(vec);
    }
    std::cout << std::endl;
}

template <typename T>
inline
typename std::vector<T>::iterator Last(std::vector<T> &vec)
{
    return (vec.end() - 1);
}

std::pair<unsigned, std::vector<unsigned>> InfiniteLoops(std::vector<unsigned> &uvec);
std::vector<unsigned>::iterator FindLargest(std::vector<unsigned>&);
void DoleOut(std::vector<unsigned>&, std::vector<unsigned>::iterator);
