/* Externally defined functions for 07Func.h
 */
#include "07Func.h"

void MakeMap(suspmap &system)
{
    std::string line;
    while (getline(std::cin, line)) {
        std::string name; 
        auto beg = line.begin(), end = line.end();
        GetAlpha(beg, end, name);

        unsigned weight = GetWeight(beg);

        std::set<std::string> ptrsfromthis;
        while (beg != end) {
            std::string ptrs;
            GetAlpha(beg, end, ptrs);
            if (!ptrs.empty()) {
                ptrsfromthis.insert(ptrs);
            }
        }
        system.insert({name, make_pair(weight, ptrsfromthis)});
    }
}

void GetAlpha(siter &beg, siter &end, std::string &temp)
{
    while (!isalpha(*beg) && (beg != end)) {
        ++beg;
    }
    while (isalpha(*beg) && (beg != end)) {
        temp += *beg;
        ++beg;
    }
}

unsigned GetWeight(siter &beg)
{
    std::string wt = "";
    while (!isdigit(*beg)) {
        ++beg;
    }
    while (isdigit(*beg)) { 
        wt += *beg; 
        ++beg;
    }
    return std::stoi(wt);
}

void Print(suspmap &system)
{
    auto beg = system.begin(), end = system.end();

    while (beg != end) {
        std::cout << beg->first << " " ;
        for (const auto &s : beg->second.second) {
            std::cout << s << " ";
        }
        std::cout << std::endl;
        ++beg;
    }
}

std::string FindRoot(suspmap &system)
{
    std::set<std::string> AllNames;
    std::set<std::string> PointedToNames;

    for (const auto &m : system) {
        AllNames.insert(m.first);
        if (!m.second.second.empty()) {
            PointedToNames.insert(m.second.second.begin(), 
                    m.second.second.end());
        }
    }

    for (const auto &s : PointedToNames) {
        AllNames.erase(s);
    }

    if (AllNames.size() == 1) {
        return *AllNames.begin();
    } else {
        return "Circular, or many roots";
    }
}

unsigned Unbalanced(suspmap &system)
{
    auto root = *(system.find(FindRoot(system)));

    ussmap checkWeights;
    unsigned odd, most;

    for (const auto &s : root.second.second) {
        checkWeights[Weight(s, system)].insert(s);
    }

    if (checkWeights.begin()->second.size() == 1) {
        odd = checkWeights.begin()->first;
        most = (++checkWeights.begin())->first;
    } else {
        most = checkWeights.begin()->first;
        odd = (++checkWeights.begin())->first;
    }

    ShowWeights(root, system);
    return FindOdd(root, system) - (odd - most);
}

unsigned FindOdd(susppair2 &p, suspmap &system)
{
    ussmap checkWeights;

    for (const auto &s : p.second.second) {
        checkWeights[Weight(s, system)].insert(s);
    }

    unsigned ret = 0;
    if (checkWeights.size() > 1) {
        for (const auto &n : checkWeights) {
            if (n.second.size() == 1) {
                std::string str = *n.second.begin();
                auto next = *(system.find(str));
                ShowWeights(next, system);
                ret = FindOdd(next, system);
            }
        }
        return ret;
    } else {
        return p.second.first;
    }
}

unsigned Weight(const std::string &name, suspmap &system)
{
    auto element = (system.find(name));
    unsigned ret = element->second.first;

    if (!element->second.second.empty()) {
        for (const auto &s : element->second.second) {
            ret += Weight(s, system);
        }
    }

    return ret;
}

void ShowWeights(const susppair &m, suspmap &system)
{
    std::cout << std::endl;
    std::cout << m.first << " " << m.second.first << std::endl;

    for (const auto &s : m.second.second) {
        std::cout << "\t" << s << " " 
           <<  Weight(s, system) << std::endl;
    }
}
