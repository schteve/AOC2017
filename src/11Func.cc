/* Externally defined functions for 11Func.h
 */
#include "11Func.h"

unsigned GetSteps()
{
    siipmap directions;
    MakeMap(directions);

    iipair pos(0, 0); // in the (ne, nw) basis
    GetPos(pos, directions);

    std::cout << "The end position is " << std::endl;
    std::cout << pos.first << " " << pos.second << std::endl;

    return GetSteps(pos, directions);
}

void MakeMap(siipmap &dir)
{
    dir.insert(std::make_pair("ne", 
                std::make_pair(1, 0)));
    dir.insert(std::make_pair("nw", 
                std::make_pair(0, 1)));

    dir.insert(std::make_pair("sw", 
                std::make_pair(-1, 0)));
    dir.insert(std::make_pair("se", 
                std::make_pair(0, -1)));

    dir.insert(std::make_pair("n", 
                std::make_pair(1, 1)));
    dir.insert(std::make_pair("s", 
                std::make_pair(-1, -1)));
}

void GetPos(iipair &pos, siipmap &dirs)
{
    unsigned moststeps = 0;

    while (std::cin) {
        char c;
        std::string compass;

        while (std::cin >> c && isalpha(c)) {
            compass += c; 
        }
        pos.first += dirs[compass].first;
        pos.second += dirs[compass].second;

        unsigned dummy = GetSteps(pos, dirs);
        if (moststeps < dummy) {
            moststeps = dummy;
        }
    }

    std::cout << "The number of steps to the path's furthest point is: " 
        << std::endl << moststeps << std::endl;
}

unsigned GetSteps(iipair pos, siipmap &dirs)
{
    unsigned count = 0;

    while (pos.first != 0 || pos.second != 0) {
        int &a = pos.first, &b = pos.second;
        if (b == 0) {
            OnAxis(a, count);
        } else if (a == 0) {
            OnAxis(b, count);
        } else if ((a/abs(a)) == (b/abs(b))) {
            if (abs(a) <= abs(b)) {
                SameSign(a, b, count);
            } else {
                SameSign(b, a, count);
            }
        } else if ((a/abs(a)) == -(b/abs(b))) {
            if (abs(a) <= abs(b)) {
                OnAxis(a, count);
            } else {
                OnAxis(b, count);
            }
        }
    }

    return count;
}
