/* 05.cc
 */

#include "05Func.h"

int main()
{
    std::vector<int> instructions;
    Read(instructions);

    std::cout << "The number of jumps required from" << std::endl;
    std::cout << "Method One is " 
        << Process(instructions, 1) << std::endl;
    std::cout << "Method Two is " 
        << Process(instructions, 2) << std::endl;

    return 0;
}
