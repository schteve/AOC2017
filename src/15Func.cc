/* Externally defined functions for 15Func.h
 */
#include "15Func.h"

unsigned Matches(unsigned long a, const unsigned &afactor,
        unsigned long b, const unsigned &bfactor)
{
    unsigned count = 0;

    for (unsigned u = 0; u < 40000000; ++u) {
        Process(a, afactor);
        Process(b, bfactor);
        //std::cout << a << " " << b << std::endl;

        Check(a, b, count);
    }
    
    return count;
}

unsigned Matches2(unsigned long a, const unsigned &afactor,
        unsigned long b, const unsigned &bfactor)
{
    unsigned count = 0;

    for (unsigned u = 0; u < 5000000; ++u) {
        Process(a, afactor);
        while (a%4 != 0) {
            Process(a, afactor);
        }
        Process(b, bfactor);
        while (b%8 != 0) {
            Process(b, bfactor);
        }
//        std::cout << a << " " << b << std::endl;

        Check(a, b, count);
    }
    
    return count;
}

void Check(const unsigned long &a, const unsigned long &b, unsigned &count)
{
    std::string abin = dectobinary(a, 32), 
                bbin = dectobinary(b, 32);
    bool bit = 0;

    for (auto beg1 = abin.end()-16, end1 = abin.end(), 
    beg2 = bbin.end()-16; beg1 != end1; ++beg1, ++beg2) {
        if (*beg1 != *beg2) {
            bit = 1;
        }
    }
    //std::cout << acmp << std::endl
     //   << bcmp << std::endl;
    if (bit == 0) {
        ++count;
    }
}

void Process(unsigned long &output, const unsigned &factor)
{
    const unsigned long &divisor = 2147483647;

    output = (output * factor) % divisor;
}

void ReadNumber(unsigned long &a)
{
    std::string line = "", number = "";
    if (std::cin) {
        getline(std::cin, line);

        for (auto beg = line.cbegin(), end = line.cend();
                beg != end; ++beg) {
            if (isdigit(*beg)) {
                number += *beg;
            }
        }
    }

    if (number != "") {
        a = std::stoull(number);
    }
}

