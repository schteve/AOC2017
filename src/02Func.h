/* 02Func.h 
 */

#include <iostream>
#include <vector>
#include <string>
#include <cctype>

int Read(std::istream&, std::vector<std::vector<int>>&);
void CheckReplace(std::string&, int&, int&, int&, bool&);
void SumReset(int&, int&, int&, bool&);
int Divisors(const std::vector<std::vector<int>>&);
