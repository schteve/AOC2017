/* 13.cc
 */

#include "13Func.h"

int main()
{
    upmap layers;
    GetMap(layers);
    //PrintMap(layers);

    auto ret = TopSeverity(layers);

    std::cout << "The severity for passing along the top is:"
        << std::endl << ret << std::endl;

    auto ret2 = GreatEscape(layers);

    std::cout << "The least time for clean run is:"
        << std::endl << ret2 << std::endl;

    return 0;
}
