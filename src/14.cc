/* 14.cc
 */

#include "14Func.h"

int main()
{
    auto ret = Squares();

    std::cout << "The total number of squares used is "
        << std::endl << ret << std::endl;

    return 0;
}
