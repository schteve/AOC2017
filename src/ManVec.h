/* ManVec.h 
 */
#ifndef MANVEC_H
#define MANVEC_H

#include <iostream>
#include <vector>
#include <initializer_list>

class ManVec;

bool operator<(const ManVec&, const ManVec&);

class ManVec {
public:
    ManVec() = default;
    ManVec(const unsigned&);
    ManVec(std::initializer_list<long> lil);

    std::ostream& Print(std::ostream&);
    std::ostream& Print(std::ostream&) const;
    void push_back(const long&);
    size_t size() 
    { return vec.size(); }
    const size_t size()  const
    { return vec.size(); }

    long& operator[](std::size_t n)
    { return vec[n]; }  
    const long& operator[](std::size_t n) const
    { return vec[n]; }  

    long def();
private:
    std::vector<long> vec = { };
};

#endif
