/* Externally defined functions for 21Func.h
 */

#include "21Func.h"

unsigned CountOn(ggmap &rules, const unsigned &n)
{
    grid art(3);
    InitArt(art);
    std::cout << "Art" << std::endl << art << std::endl;

    for (unsigned u = 0; u < n; ++u) {
        Process(art, rules);

        if (n <= 5) {
        std::cout << "Art" << std::endl << art << std::endl;
        } else {
            std::cout << u << " ";
        }
    }
    std::cout << std::endl;

    unsigned count = 0;
    for (unsigned u = 0; u < art.height()*art.height(); ++u) {
        count += art[u];
    }

    return count;
}

void Process(grid &art, ggmap &rules)
{
    unsigned width = art.height();
    if (width % 2 == 0) {
        ProSize(art, rules, 2);
    } else if (width % 3 == 0) {
        ProSize(art, rules, 3);
    }
}

void ProSize(grid &art, ggmap &rules, const unsigned &sz)
{
    unsigned width = art.height();
    unsigned tempwidth = width + width/sz;
    grid temp(tempwidth);
    unsigned nosubs = width / sz;

    unsigned x = 0, y = 0, X = 0, Y = 0;
    for (unsigned n = 0; n < nosubs; ++n) {
        for (unsigned m = 0; m < nosubs; ++m) {
            grid sub = SubGrid(art, sz, x, y);
            grid res = rules[sub];
            AssignSub(temp, res, X, Y);
            x += sz;
            if (x >= width) {
                x = 0;
            }
            X += sz + 1;
            if (X >= tempwidth) {
                X = 0;
            }
        }
        y += sz;
        Y += sz + 1;
    }
    art = temp;
}

void MakeMap(ggmap &r)
{
    std::string line;
    while (getline(std::cin, line)) {
        grid from, to;
        Read(from, to, line);
        Insert(r, from, to);
    }
}

void Insert(ggmap &r, const grid &f, const grid &t)
{
    grid temp;
    temp = Rotate(f);
    RotThree(r, temp, t);

    temp = Mirror(f); 
    RotThree(r, temp, t);
}

void Read(grid &f, grid &t, const std::string &line)
{
    std::vector<unsigned> uvec = GetBinary(line);
    unsigned fsize = 0, tsize = 0;
    FindSizes(uvec, fsize, tsize); 
    grid tf(fsize), tt(tsize);

    GridAssign(tf, uvec);
    GridAssign(tt, std::vector<unsigned>(
                uvec.begin() + fsize * fsize, uvec.end()));

    f = tf;
    t = tt;
}
