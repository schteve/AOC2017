/* 01Func.h 
 */

#include <iostream>
#include <string>

using str_ci = std::string::const_iterator;

unsigned eval(const std::string&, unsigned&);
void oppoinc(str_ci&, const str_ci&, const unsigned &, 
        unsigned&);
inline unsigned char_to_int(std::string::const_iterator i)
{
    return *i - '0';
}
