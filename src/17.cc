/* 17.cc
 */

#include "17Func.h"

int main()
{
    unsigned long skip;

    std::cin >> skip;

    Result("The number after 2017 after 2017 iterations is", 
            spinlock(skip));

    Result("The number after 0 after 5*10^7 iterations is", 
            Second(skip));

    return 0;
}
