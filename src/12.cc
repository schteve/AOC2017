/* 12.cc
 */

#include "12Func.h"

int main()
{
    uusmap pipes;
    GetMap(pipes);
    //PrintMap(pipes);

    auto ret = CountElements(pipes, 0);
    std::cout << "The size of the zero group is:" 
        << std::endl << ret.size() << std::endl;

    auto rot = CountGroups(pipes);
    std::cout << "The number of groups is:"
        << std::endl << rot << std::endl;

    return 0;
}
