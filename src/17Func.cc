/* Externally defined functions for 17Func.h
 */

#include "17Func.h"

unsigned long Second(const unsigned long &skip)
{
    unsigned long size = 1, curr = 0, largest = 0;

    for ( ; size < 50000000; ++size) {
        curr = (curr + skip)%size;
        if (curr == 0) {
            largest = size;
        }
        ++curr;
    }

    return largest;
}

unsigned spinlock(const unsigned long &skip)
{
    std::vector<unsigned> uvec = { 0 };
    unsigned long curr = 0;

    for (unsigned u = 1; u < 2018; ++u) {
        curr = (curr + skip)%uvec.size();

        std::vector<unsigned> temp(uvec.begin(),
                uvec.begin() + curr + 1);
        temp.push_back(u);
        temp.insert(temp.end(), uvec.begin() + curr + 1,
                uvec.end());

        uvec = temp;
        ++curr;
        /*
        Print(curr, uvec);
        std::cout << std::endl;
        */
    }
    //Print(curr, uvec);

    return uvec[++curr];
}
