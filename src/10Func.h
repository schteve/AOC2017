/* 10Func.h 
 */

#include <iostream>
#include <sstream>
#include <string>
#include <memory>

std::string Hash(std::istream&);
std::string Hash(const std::string&);
std::string Densify(unsigned*, const unsigned&);
void ProcessHash(const std::string&, unsigned*, const unsigned&);
void InitSubset(unsigned*, const unsigned&, unsigned*, unsigned, 
        const unsigned&);
void RevSet(unsigned*, const unsigned&, unsigned*, unsigned, const unsigned&);

inline
std::string Hex(const unsigned &dummy)
{
    if (dummy < 10) {
        return std::to_string(dummy);
    } else if (dummy < 16) {
        char c = dummy + 87;
        std::string s;
        s += c;
        return s;
    } else {
        return "?";
    }
}

inline
std::string ToHex(const unsigned &dec) 
{
    return Hex(dec/16) + Hex(dec%16);
}

inline
void InitArray(unsigned* beg, const unsigned &length)
{
    for (unsigned u = 0; u < length; ++u) {
        *(beg + u) = u;
    }
}

inline 
void PrintArray(unsigned* arr, const unsigned &length)
{
    for (unsigned *beg = arr; beg < arr + length; ++beg) {
        std::cout << *beg << " ";
    }
    std::cout << std::endl;
}

inline
std::string IpPro(const std::string &dirty)
{
    std::istringstream iss(dirty);
    std::string bytes;
    char c;
    while (iss >> c) {
        int i = c;
        bytes += std::to_string(i) + ",";
    }

    return bytes += "17,31,73,47,23";
}

inline
void GetInputString(std::istream &is, std::string &s)
{
    std::string dummy, dirty;
    while (getline(is, dummy)) {
        dirty += dummy;
    }
    s = IpPro(dirty);
}

inline
unsigned GetNextLength(std::istream &is)
{
    char c;
    std::string num;
    while ((is >> c) && (c != ',')) {
        num += c;
    } 
    return std::stoi(num);
}
